# Grow# #

##Grow Sharp Maximum Power Point Tracking Node.

This repository has the EFM32ZeroGecko files for the mppt node. This node is based on a EFM32ZeroGecko board from SiliconLabs and has all the files included in the SimplicityStudio project.

This board is the main controller of the photo-voltaic panel. It read current, voltage and power of the PV panel to maximize its efficiency and and monitors the battery status. The pcb layout developed on Kicad workspace is [see here](https://bitbucket.org/jmbranco76/growsharp_mppt_kicad).

Board:  Silicon Labs EFM32ZG-STK3200 Development Kit and Sensor Add-on board
Device: EFM32ZG222F32
