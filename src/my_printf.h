/**
  ******************************************************************************
  * @file    my_printf.h
  * @author  Jos� Marques
  * @version V1.0.0
  * @date    20/12/2013
  * @brief   my_printf file
  ******************************************************************************
  */

#ifndef MY_PRINTF_H_
#define MY_PRINTF_H_

#include <stdint.h>

int my_sprintf(char *out, const char *format, ...);
void printSolar(double v, double a, double p);

#endif /* MY_PRINTF_H_ */
