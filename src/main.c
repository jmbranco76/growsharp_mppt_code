/**************************************************************************//**
 * @file
 * @brief Grow Sharp Maximum Power Point Tracking Node
 * @version 4.0.0
 ******************************************************************************
 * @section License
 * <b>(C) Copyright 2014 Silicon Labs, http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/
#include <stdio.h>
#include "em_device.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_emu.h"
#include "em_leuart.h"
#include "em_gpio.h"
#include "i2cspm.h"
#include "si7013.h"
#include "si114x_algorithm.h"
#include "rtcdriver.h"
#include "em_adc.h"
#include "bspconfig.h"
#include "ina2xx.h"
#include "bsp.h"
#include "glib.h"
#include "em_timer.h"

#include "display.h"
#include "textdisplay.h"
#include "retargetserial.h"
//#include "retargetvirtual.h"

#include "my_printf.h"

///**************************************************************************//**
// * Local defines
// *****************************************************************************/
///** Time (in ms) to keep looking for gestures if none are seen. */
//#define GESTURE_TIMEOUT_MS      60000


// PWM Defines
#define PWM_PORT  gpioPortA
#define PWM_PIN   0

#define TIMER_TOP                   100
#define DUTY_CYCLE                  1
#define TIMER_CHANNEL               0

/**************************************************************************//**
 * Local variables
 *****************************************************************************/
/* RTC callback parameters. */
static void (*rtcCallback)(void*) = NULL;
static void * rtcCallbackArg = 0;

/** Millisecond tick counter */
volatile uint32_t msTicks;

uint32_t v=0;

static GLIB_Context_t   glibContext;


/** Timer used for timing out gesturemode to save power. */
RTCDRV_TimerID_t gestureTimeoutTimerId;
/** Timer used for counting milliseconds. Used for gesture detection. */
RTCDRV_TimerID_t millisecondsTimerId;
/** Timer used for periodic update of the measurements. */
RTCDRV_TimerID_t periodicUpdateTimerId;
/** Timer used for animations (swiping) */
RTCDRV_TimerID_t animationTimerId;

/**************************************************************************//**
 * Local prototypes
 *****************************************************************************/
//static void gpioSetup(void);
static void periodicUpdateCallback(RTCDRV_TimerID_t id, void *user);


void printSolar(double v, double a, double p)
{

	GLIB_drawString(&glibContext, "Solar Panel", 11, 0, 10, true);
	char text[50];
	int unidades, centezimas;
	GLIB_setFont(&glibContext, &GLIB_FontNormal8x8);
	unidades = (int)v;
	centezimas = ((int)(v*100)%100);
	my_sprintf(text, "Vol.= %02d.%02d V", unidades, abs(centezimas));
	GLIB_drawString(&glibContext, text, strlen(text), 0, 20, true);
	unidades = (int)a;
	centezimas = ((int)(a*100)%100);
	my_sprintf(text, "Curr= %02d.%02d A", unidades, abs(centezimas));
	GLIB_drawString(&glibContext, text, strlen(text), 0, 30, true);
	unidades = (int)p;
	centezimas = ((int)(p*100)%100);
	my_sprintf(text, "Pow.= %03d.%02d W", unidades, abs(centezimas));
	GLIB_drawString(&glibContext, text, strlen(text), 0, 40, true);
	DMD_updateDisplay();
}

void printBattery(double v, double a, double p)
{

	GLIB_drawString(&glibContext, "Battery", 13, 0, 55, true);
	char text[50];
	int unidades, centezimas;
	GLIB_setFont(&glibContext, &GLIB_FontNormal8x8);
	unidades = (int)v;
	centezimas = ((int)(v*100)%100);
	my_sprintf(text, "Vol.= %02d.%02d V", unidades, abs(centezimas));
	GLIB_drawString(&glibContext, text, strlen(text), 0, 65, true);
	unidades = (int)a;
	centezimas = ((int)(a*100)%100);
	my_sprintf(text, "Curr= %02d.%02d A", unidades, abs(centezimas));
	GLIB_drawString(&glibContext, text, strlen(text), 0, 75, true);
	unidades = (int)p;
	centezimas = ((int)(p*100)%100);
	my_sprintf(text, "Pow.= %03d.%02d W", unidades, abs(centezimas));
	GLIB_drawString(&glibContext, text, strlen(text), 0, 85, true);
	DMD_updateDisplay();
}
void printPWM(int pwm)
{
	char text[50];
	my_sprintf(text, "Pwm.= %03d", pwm);
	GLIB_drawString(&glibContext, text, strlen(text), 0, 95, true);
	DMD_updateDisplay();
}

void printSolarToUart(double v, double a, double p)
{
//	printf("Solar Measurements\n");
//	char text[50];
//	int unidades, centezimas;
//	//GLIB_setFont(&glibContext, &GLIB_FontNormal8x8);
//	unidades = (int)v;
//	centezimas = ((int)(v*100)%100);
//	my_sprintf(text, "Vol.= %02d.%02d V", unidades, abs(centezimas));
//	printf("%s \n",text);
//	unidades = (int)a;
//	centezimas = ((int)(a*100)%100);
//	my_sprintf(text, "Curr= %02d.%02d A", unidades, abs(centezimas));
//	printf("%s \n",text);
//	unidades = (int)p;
//	centezimas = ((int)(p*100)%100);
//	my_sprintf(text, "Pow.= %03d.%02d W", unidades, abs(centezimas));
//	printf("%s \n",text);

//	printf("Solar Measurements\n");
	char text[50];
	int unidades, centezimas;
	//GLIB_setFont(&glibContext, &GLIB_FontNormal8x8);
	unidades = (int)(v*1000);
	//centezimas = ((int)(v*100)%100);
	//my_sprintf(text, "Vol.= %02d.%02d V", unidades, abs(centezimas));
	my_sprintf(text, "PV=%04d", unidades);
	//printf("PV-V=%dmV, A=%d, P=%d\n", value, value2,value3);
    //printf("PV-V=%dmV,", unidades);
	printf("%s,",text);
	unidades = (int)(a*1000);
//	centezimas = ((int)(a*100)%100);
//	my_sprintf(text, "A= %04d", unidades);
	my_sprintf(text, "%04d", unidades);
//	printf("%s \n",text);
	printf("%s,", text);
	unidades = (int)(p*1000);
//	centezimas = ((int)(p*100)%100);
	//my_sprintf(text, "Pow.= %04d", unidades);
	my_sprintf(text, "%04d", unidades);
	printf("%s \r\n",text);
}

void LogUART(double sv, double sa, double sp, double bv, double ba, double bp, double pwm)
{
static int i = 0;
char text[50];
	if (i==0)
	{
	printf("Svolt,Scurr,Spower,Bvolt,Bcurr,Bpower,PWM\n");
	i=i+1;
	}

//	printf("@");
	int unidades, centezimas;
	//GLIB_setFont(&glibContext, &GLIB_FontNormal8x8);
	unidades = (int)sv;
	centezimas = ((int)(sv*100)%100);
	my_sprintf(text, "%02d.%02d", unidades, abs(centezimas));
	printf("%s ,",text);
	unidades = (int)sa;
	centezimas = ((int)(sa*100)%100);
	my_sprintf(text, "%02d.%02d", unidades, abs(centezimas));
	printf("%s ,",text);
	unidades = (int)sp;
	centezimas = ((int)(sp*100)%100);
	my_sprintf(text, "%03d.%02d", unidades, abs(centezimas));
	printf("%s ,",text);
	unidades = (int)bv;
	centezimas = ((int)(bv*100)%100);
	my_sprintf(text, "%02d.%02d", unidades, abs(centezimas));
	printf("%s ,",text);
	unidades = (int)ba;
	centezimas = ((int)(ba*100)%100);
	my_sprintf(text, "%02d.%02d", unidades, abs(centezimas));
	printf("%s ,",text);
	unidades = (int)bp;
	centezimas = ((int)(bp*100)%100);
	my_sprintf(text, "%03d.%02d", unidades, abs(centezimas));
	printf("%s ,",text);
	unidades = (int)pwm;
	my_sprintf(text, "%d", unidades);
	printf("%s\n",text);
	printf("PV=%d,%d,%d\r\n",(int)(sv*1000),(int) (sa*1000),(int)(sp*1000));
	printf("BAT=%d,%d,%d\r\n",(int)(bv*1000),(int) (ba*1000),(int)(bp*1000));
}

void printBatteryToUart(double v, double a, double p)
{

	printf("Battery Measurements\n");
	char text[50];
	int unidades, centezimas;
	//GLIB_setFont(&glibContext, &GLIB_FontNormal8x8);
	unidades = (int)v;
	centezimas = ((int)(v*100)%100);
	my_sprintf(text, "Vol.= %02d.%02d V", unidades, abs(centezimas));
	printf("%s \n",text);
	unidades = (int)a;
	centezimas = ((int)(a*100)%100);
	my_sprintf(text, "Curr= %02d.%02d A", unidades, abs(centezimas));
	printf("%s \n",text);
	unidades = (int)p;
	centezimas = ((int)(p*100)%100);
	my_sprintf(text, "Pow.= %03d.%02d W", unidades, abs(centezimas));
	printf("%s \n",text);
}




/**************************************************************************//**
 * @brief  Main function
 *****************************************************************************/
int main(void)
{
	SysTick_Config(CMU_ClockFreqGet(cmuClock_CORE) / 1000);//For Delay routine

	//	if (SysTick_Config(CMU_ClockFreqGet(cmuClock_CORE) / 1000))
	//	      {
	//	            DEBUG_BREAK;
	//	      }


	I2CSPM_Init_TypeDef i2cInit = I2CSPM_INIT_DEFAULT;

	uint32_t i = 0;
	bool a;
	uint16_t pwm=10;

	/* Chip errata */
	CHIP_Init();

	/* PWM initalizations. */
	CMU_ClockEnable(cmuClock_GPIO, true);
	CMU_ClockEnable(cmuClock_TIMER0, true);
	// Enable LED output
	GPIO_PinModeSet(PWM_PORT, PWM_PIN, gpioModePushPull, 0);
	// Create the timer count control object initializer
	TIMER_InitCC_TypeDef timerCCInit = TIMER_INITCC_DEFAULT;
	timerCCInit.mode = timerCCModePWM;
	timerCCInit.cmoa = timerOutputActionToggle;
	// Configure CC channel 0
	TIMER_InitCC(TIMER0, TIMER_CHANNEL, &timerCCInit);
	// Route CC2 to location 0 (PA0) and enable pin for cc0
	TIMER0->ROUTE |= (TIMER_ROUTE_CC0PEN | TIMER_ROUTE_LOCATION_LOC0);
	// Set Top Value
	TIMER_TopSet(TIMER0, TIMER_TOP);
	// Set the PWM duty cycle here!
	TIMER_CompareBufSet(TIMER0, TIMER_CHANNEL, DUTY_CYCLE);
    // Create a timerInit object, based on the API default
    TIMER_Init_TypeDef timerInit = TIMER_INIT_DEFAULT;
    timerInit.prescale = timerPrescale2;
    TIMER_Init(TIMER0, &timerInit);

	/* Misc timers. */
	RTCDRV_Init();
	RTCDRV_AllocateTimer(&gestureTimeoutTimerId);
	RTCDRV_AllocateTimer(&millisecondsTimerId);
	RTCDRV_AllocateTimer(&periodicUpdateTimerId);
	RTCDRV_AllocateTimer(&animationTimerId);


	/* Initialize USART and map LF to CRLF */
	RETARGET_SerialInit();
	printf("teste usart\n");

	/* Initialize I2C driver, using standard rate. */
	I2CSPM_Init(&i2cInit);

	EMSTATUS status;
		/* Initialize the display module. */
		status = DISPLAY_Init();
		if (DISPLAY_EMSTATUS_OK != status)
			while(1);

		/* Initialize the DMD module for the DISPLAY device driver. */
		status = DMD_init(0);
		if (DMD_OK != status)
			while(1);

		status = GLIB_contextInit(&glibContext);
		if (GLIB_OK != status)
			while(1);

		/* Setup display colours */
		glibContext.backgroundColor = White;
		glibContext.foregroundColor = Black;
		GLIB_clear(&glibContext);

	uint16_t conf_reg=0 ;
	// INA209 config
	ina209_set(I2C0,0x00, 0x3FFF);
	ina209_get(I2C0,0x00,&conf_reg);

	ina209_set(I2C0,0x16, 54612);
	ina209_get(I2C0,0x16,&conf_reg);

	/// INA226 config
	ina226_set(I2C0,0x00, 0x4927);
	ina226_get(I2C0, 0x00, &conf_reg);
	conf_reg = conf_reg+1;

	ina226_set(I2C0,0x05, 0x0D55);
	ina226_get(I2C0,0x05, &conf_reg);

	float solar_voltage, solar_current, solar_power;
	float battery_voltage, battery_current, battery_power;
	while (1)

	{

		if(i > 100000)
		{
			i=0;
			//printf("Solar Measurements\n");
			//LEUART_Tx(&leuart,65);
//			solar_voltage = ina226GetVoltage(I2C0, NULL);
//			solar_current = ina226GetCurrent(I2C0, NULL);
//			solar_power = solar_voltage * solar_current;
//			printSolar(solar_voltage, solar_current, solar_power);
			//printSolarToUart(solar_voltage, solar_current, solar_power);
			//printf ("Solar Voltage: %f\n",solar_voltage );

//			battery_voltage = ina209GetVoltage(I2C0, NULL);
//			battery_current = ina209GetCurrent(I2C0, NULL);
//			battery_power = battery_voltage * battery_current;
//			printBattery(battery_voltage, battery_current, battery_power);
			//printBatteryToUart(battery_voltage, battery_current, battery_power);
			//printPWM(pwm);
//			LogUART(solar_voltage, solar_current, solar_power, battery_voltage, battery_current, battery_power,pwm);
			//printf("PWM:%d \n",pwm);
//			TIMER_CompareBufSet(TIMER0, TIMER_CHANNEL,pwm);
//			if (pwm < 50)
//			pwm++;


			// Perturb and Observe Alghorithm
//			uint8_t state = update_mppt(pwm);
			update_mppt(&pwm);
			//UARTprintf("pwm=%d,state = %d\n", pwm2, state);
			//UARTprintf("%d,%d\n", pwm2, state);

		}
i=i+1;
	}
}


int update_mppt(uint16_t *pwm_ptr)

{
	static int state = 0;
	static uint16_t pwm=10;
	*pwm_ptr = pwm;
	static float battery_voltage_limit = 15;
//	float battery_voltage = ina209GetVoltage(I2C0, NULL);
	static float battery_voltage = 0;
	static float battery_current = 0;
	static float battery_power = 0;

//	int valor;
//	int voltage_limit_ = 60;
//	float solar_voltage = ina226GetVoltage(I2C0, NULL);
//	float solar_current = ina226GetCurrent(I2C0, NULL);
	static float last_power = 0;
	int solar_voltage_limit = 30;
	static float solar_voltage=0;
	static float solar_current=0;
	float solar_power=0;
	static uint16_t up=1;

//	for	(int i=0;i<=5;i++ )
//	{
	solar_voltage = ina226GetVoltage(I2C0, NULL);
//	solar_voltage /=2;
	//Delay(10);
	for	(int i=0;i<=5;i++ )
	{
	solar_current +=ina226GetCurrent(I2C0, NULL);
	solar_current /=2;
	}
	solar_power = solar_voltage * solar_current;
//	solar_power /= 2;
	printSolar(solar_voltage, solar_current, solar_power);
	//printSolarToUart(solar_voltage, solar_current, solar_power);

	battery_voltage = ina209GetVoltage(I2C0, NULL);
	battery_current = ina209GetCurrent(I2C0, NULL);
	battery_power = battery_voltage * battery_current;
	printBattery(battery_voltage, battery_current, battery_power);
	printPWM(pwm);
	//LogUART(solar_voltage, solar_current, solar_power, battery_voltage, battery_current, battery_power,pwm);
//	}
//	solar_power=solar_power /5;

//	if(solar_power >= last_power)
//	{
//		if (pwm <= 99 && battery_voltage < battery_voltage_limit )
//			pwm=pwm+1;
//		else
//			pwm=pwm-1;
//	}
//
//	else
//	{
//		if (solar_voltage <= solar_voltage_limit)
//		{
//			if (pwm >=1)pwm-=1;
//		}
//	}

	if(solar_power < last_power)
	{
		up ^=1; //Toogle direction
	}

	if (!up)
	{
		if (battery_voltage <= battery_voltage_limit )
		{
			if (pwm<=98)pwm=pwm+2;
		}
//		else
//		{
//			if(pwm>=1)pwm=pwm-1;
//			up=0;
//		}

	}
	else
	{
		if(pwm>=2)
		{
		pwm=pwm-2;
		}
	}

 //pwm=15;

	LogUART(solar_voltage, solar_current, solar_power, battery_voltage, battery_current, battery_power,pwm);







//
//	switch (state) {
//	case 0:
//		pwm = 0;
//		Delay(100);
//		state = 2;
//		break;
//
//	case 1:		// increase operation voltage
//		if (pwm>=5  && solar_voltage < solar_voltage_limit)
//		pwm-=5;
//		if(solar_power >= last_power)
//		{
//			state = 1;
//		}
//		else
//		{
//			state = 2;
//		}
//		break;
//
//	case 2:		// decrease Operation voltage
////		if (pwm <=95 && battery_voltage < battery_voltage_limit)
////		pwm+=5;
//		if(solar_power >= last_power)
//		{
//			if (pwm <=95 && battery_voltage < battery_voltage_limit)
//				pwm+=5;
//			//UARTprintf("solar_power > last_power");
//			state = 2;
//		}
//		else
//		{
//			//UARTprintf("solar_power < last_power");
//			state = 1;
//		}
//		break;
//
//	default:
//		break;
//	}

	if (pwm>100) pwm=100;
	if (pwm<0) pwm=0;
	TIMER_CompareBufSet(TIMER0, TIMER_CHANNEL,pwm);

	last_power = solar_power;

	return state;

}


void Delay(uint32_t dlyTicks)
{
	uint32_t curTicks;

	curTicks = msTicks;
	while ((msTicks - curTicks) < dlyTicks) ;
}

void SysTick_Handler(void)
{
	/* Increment counter necessary in Delay()*/
	msTicks++;
}


