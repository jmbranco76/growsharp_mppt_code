/*
 * ina2xx.c
 *
 *  Created on: 26 de Set de 2015
 *      Author: Jos�
 */


#include "ina2xx.h"

void init_i2c()
{

	I2CSPM_Init_TypeDef i2cInit = I2CSPM_INIT_DEFAULT;
	I2CSPM_Init(&i2cInit);

}

bool ina226_get(I2C_TypeDef *i2c, uint8_t ina_addr, uint16_t *value)
{
	I2C_TransferSeq_TypeDef    seq;
	I2C_TransferReturn_TypeDef ret;
	uint8_t                    i2c_read_data[2];
	uint8_t                    i2c_write_data;

	seq.addr  = INA226_ADDR;
	seq.flags = I2C_FLAG_WRITE_READ;
	/* Select command to issue */
	i2c_write_data = ina_addr;
	seq.buf[0].data   = &i2c_write_data;
	seq.buf[0].len    = 1;
	/* Select location/length of data to be read */
	seq.buf[1].data = i2c_read_data;
	seq.buf[1].len  = 2;

	ret = I2CSPM_Transfer(i2c, &seq);
	if (ret != i2cTransferDone)
	{
		*value = 0;
		return false;
	}
	*value = (i2c_read_data[0] << 8) | i2c_read_data[1];
	return true;
}

bool ina226_set(I2C_TypeDef *i2c, uint8_t ina_addr, uint16_t value)
{

	I2C_TransferSeq_TypeDef    seq;
	I2C_TransferReturn_TypeDef ret;
	uint8_t                    i2c_read_data[3];
	uint8_t                    i2c_write_data[3];

	seq.addr  = INA226_ADDR;
	seq.flags = I2C_FLAG_WRITE;
	/* Select command to issue */
	i2c_write_data[0] = ina_addr;
	i2c_write_data[1] = value >> 8;
	i2c_write_data[2] = value & 0xFF;
	seq.buf[0].data   = i2c_write_data;
	seq.buf[0].len    = 3;
	/* Select location/length of data to be read */
	seq.buf[1].data = i2c_read_data;
	seq.buf[1].len  = 0;

	ret = I2CSPM_Transfer(i2c, &seq);

	if (ret != i2cTransferDone)
	{
		return false;
	}

	return true;
}

bool ina209_get(I2C_TypeDef *i2c, uint8_t ina_addr, uint16_t *value)
{
	I2C_TransferSeq_TypeDef    seq;
	I2C_TransferReturn_TypeDef ret;
	uint8_t                    i2c_read_data[2];
	uint8_t                    i2c_write_data;

	seq.addr  = INA209_ADDR;
	seq.flags = I2C_FLAG_WRITE_READ;
	/* Select command to issue */
	i2c_write_data = ina_addr;
	seq.buf[0].data   = &i2c_write_data;
	seq.buf[0].len    = 1;
	/* Select location/length of data to be read */
	seq.buf[1].data = i2c_read_data;
	seq.buf[1].len  = 2;

	ret = I2CSPM_Transfer(i2c, &seq);
	if (ret != i2cTransferDone)
	{
		*value = 0;
		return false;
	}
	*value = (i2c_read_data[0] << 8) | i2c_read_data[1];
	return true;
}

bool ina209_set(I2C_TypeDef *i2c, uint8_t ina_addr, uint16_t value)
{

	I2C_TransferSeq_TypeDef    seq;
	I2C_TransferReturn_TypeDef ret;
	uint8_t                    i2c_read_data[3];
	uint8_t                    i2c_write_data[3];

	seq.addr  = INA209_ADDR;
	seq.flags = I2C_FLAG_WRITE;
	/* Select command to issue */
	i2c_write_data[0] = ina_addr;
	i2c_write_data[1] = value >> 8;
	i2c_write_data[2] = value & 0xFF;
	seq.buf[0].data   = i2c_write_data;
	seq.buf[0].len    = 3;
	/* Select location/length of data to be read */
	seq.buf[1].data = i2c_read_data;
	seq.buf[1].len  = 0;

	ret = I2CSPM_Transfer(i2c, &seq);

	if (ret != i2cTransferDone)
	{
		return false;
	}

	return true;
}

float ina209GetVoltage(I2C_TypeDef *i2c, bool *ret_op)
{
	uint16_t conf_reg;
	bool ret = ina209_get(i2c, 0x04, &conf_reg);
	*ret_op = ret;
	float voltage;
	if(ret)
	{
		voltage = (conf_reg>>3);
		voltage *=0.004;
		return voltage;
	}
	return 0;
}

float ina209GetShuntVoltage(I2C_TypeDef *i2c, bool *ret_op)
{
	int16_t shunt_voltage;
	bool ret = ina209_get(i2c, 0x03, (uint16_t *)&shunt_voltage);
	*ret_op = ret;
	float voltage;
	if(ret)
	{
		voltage = (shunt_voltage);
		voltage *= 0.01;
		return voltage;
	}
	return 0;
}

float ina209GetCurrent(I2C_TypeDef *i2c, bool *ret_op)
{
	int16_t curr;
	bool ret = ina209_get(i2c, 0x06, &curr);
	*ret_op = ret;
	float current;
	if(ret)
	{
		current = (curr);
		current *= 0.0001724;
		return current;
	}
	return 0;
}

float ina226GetVoltage(I2C_TypeDef *i2c, bool *ret_op)
{
	int16_t conf_reg;
	bool ret = ina226_get(i2c, 0x02, (uint16_t *)&conf_reg);
	float voltage;
	if(ret)
	{
		voltage = (conf_reg);
		voltage *= 0.00125;
		return voltage;
	}
	return 0;
}

float ina226GetShuntVoltage(I2C_TypeDef *i2c, bool *ret_op)
{
	int16_t conf_reg;
	bool ret = ina226_get(i2c, 0x01, (uint16_t *)&conf_reg);
	float voltage;
	if (ret)
	{
		voltage = (conf_reg);
		voltage *= 0.0025;
		return voltage;
	}
	return 0;
}

float ina226GetCurrent(I2C_TypeDef *i2c, bool *ret_op)
{
	int16_t conf_reg;
	bool ret = ina226_get(i2c, 0x04, (uint16_t *)&conf_reg);
	float current;
	if(ret)
	{
		current = conf_reg;
		current *= 0.0014285;
		return -current;
	}
	return 0;
}
