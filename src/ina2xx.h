/*
 * ina2xx.h
 *
 *  Created on: 26 de Set de 2015
 *      Author: Jos�
 */

#ifndef INA2XX_H_
#define INA2XX_H_

#include "stddef.h"
#include "em_system.h"
#include "em_device.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_emu.h"
#include "em_gpio.h"
#include "i2cspm.h"
#include "bspconfig.h"

#define INA209_ADDR		0x80
#define INA226_ADDR		0x8A

void init_i2c();
bool ina226_get(I2C_TypeDef *i2c, uint8_t ina_addr, uint16_t *value);
bool ina226_set(I2C_TypeDef *i2c, uint8_t ina_addr, uint16_t value);
bool ina209_get(I2C_TypeDef *i2c, uint8_t ina_addr, uint16_t *value);
bool ina209_set(I2C_TypeDef *i2c, uint8_t ina_addr, uint16_t value);
float ina209GetVoltage(I2C_TypeDef *i2c, bool *ret_op);
float ina209GetShuntVoltage(I2C_TypeDef *i2c, bool *ret_op);
float ina209GetCurrent(I2C_TypeDef *i2c, bool *ret_op);
float ina226GetVoltage(I2C_TypeDef *i2c, bool *ret_op);
float ina226GetShuntVoltage(I2C_TypeDef *i2c, bool *ret_op);
float ina226GetCurrent(I2C_TypeDef *i2c, bool *ret_op);

#endif /* INA2XX_H_ */
